#include "textflag.h"

// func hash_uint64(p unsafe.Pointer, h uint64) uint64
// Copy of runtime.memhash64
TEXT ·hash_uint64(SB),NOSPLIT,$0-24
  CMPB runtime·useAeshash(SB), $0
  JEQ noaes
  MOVQ p+0(FP), AX // ptr to data
  MOVQ h+8(FP), X0 // seed
  PINSRQ $1, (AX), X0 // data
  AESENC runtime·aeskeysched+0(SB), X0
  AESENC runtime·aeskeysched+16(SB), X0
  AESENC runtime·aeskeysched+32(SB), X0
  MOVQ X0, ret+16(FP)
  RET
noaes:
  JMP runtime·memhash64Fallback(SB)

