package amap

import (
  "errors"
  "reflect"
)

// This sets an entry in the map. If the map is typed,
// it will return an error if the key does not match
// the key type, or if they value does not match
// the value type. If the map is not typed, then this
// wil return an error of the key cannot be hashed.
func (a *AMap) Set(key, val interface{}) error {
  if a.force_type {
    if reflect.TypeOf(key) != a.key_type {
      return errors.New("Key type is incorrect!")
    }
    if reflect.TypeOf(val) != a.val_type {
      return errors.New("Value type is incorrect!")
    }
  }
  h, err := Hash(key)
  if err != nil { return err }

  // This first call should be able to grow the map.
  a.set_hash(h, key, val)

  return nil
}

// This takes a hashed key, and the actual key and value.
// This is used internally so that set retries don't need
// to hash the key multiple times.
func (a *AMap) set_hash(hash uint64, key, val interface{}) {
  // Lock map data for editing, not resizing
  a.rw.RLock()
  data := a.load_data()
  entry := data.list[data.index_of_hash(hash)]

  if entry.set(hash, key, val) {
    data.add_len()
  }

  // If the map is more than 50% full,
  // we double the capacity
  if data.Len() > data.Cap() / 2 {
    a.rw.RUnlock()
    a.grow()
  } else {
    // Need to unlock after checking data.Len()
    a.rw.RUnlock()
  }
}

// This will return an error if the key or value type is wrong.
// If the key already exists, it will return false, and it won't
// modify the map.
func (a *AMap) SetIfAbsent(key, value interface{}) (modified bool, err error) {
  return false, nil
}

// This removes an entry from the map. If the key cannot be hashed,
// or if the key was not in the map, it returns nil and false.
// Otherwise, it will return the item that was previously in the map,
// and true.
// WIP: Changed data structure
//func (a *AMap) Del(key interface{}) (val interface{}, ok bool) {
//  h, err := Hash(key)
//  if err != nil { return nil, false }
//
//  data := a.wait_load_data()
//  elem, i := data.entry(h)
//
//  // Walk the linked list
//  var prev *map_value
//  for elem != nil {
//    if Equal(elem.key, key) {
//      if prev == nil {
//        // Means elem is the first item in the list
//        data.list[i] = elem.load_next()
//      } else {
//        prev.set_next(elem.load_next())
//      }
//      // Len -= 1
//      atomic.AddUint64(&a.len, ^uint64(0))
//      // Elem is now out of scope, we can return
//      return elem.val, true
//    }
//    prev = elem
//    elem = elem.load_next()
//  }
//
//  return nil, false
//}
