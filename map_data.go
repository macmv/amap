package amap

import (
  "sync"
  "unsafe"
  "sync/atomic"
)

// This is the map list, and
// the keysize of the map list.
// This is it's own struct because
// it needs to be accessed atomically.
type map_data struct {
  // List of map items. Sorted by hash.
  list []*map_entry

  // Number of bits to read from in the key.
  // 2^keysize is the capacity of the map.
  keysize uint8

  // Length of the map
  len uint64
}

// This is an item in the list of the map. This has a mutex, and
// a linked list. That linked list should only be used when the
// mutex is locked. However, it is safe to atomically check that
// linked list agains nil, to see if this entry is empty (makes lookups faster).
type map_entry struct {
  // Used to synronize this element.
  sync.Mutex

  // Pointer to a map_value or nil.
  values unsafe.Pointer
}

// This is a value within the map. It is an item of a linked list.
// The first element in the linked list is store in a map_entry.
// Any time this is modified, the mutex in the map_entry should
// be locked.
type map_value struct {
  // This will never be accessed atomically, as the map_entry will be
  // locked when this is used.
  next *map_value

  // Cached hash of the key
  hash uint64
  // The actual key in the map
  key interface{}
  // The actual value in the map
  val interface{}
}

// This converts a hash to an index, given the keysize of m.
func (m *map_data) index_of_hash(h uint64) (uint64) {
  // 1 << cap is 2^cap
  // subtracting 1 sets all bits less than cap to 1
  // So if cap is 3, then this will produce 7 (3 ones in binary)
  return h & (1 << m.keysize - 1)
}

// Returns the number of items in the map
func (m *map_data) Len() uint64 {
  return atomic.LoadUint64(&m.len)
}

func (m *map_data) add_len() {
  atomic.AddUint64(&m.len, 1)
}

// Returns the capacity of the map data.
func (m *map_data) Cap() uint64 {
  return 1 << m.keysize
}

func (e *map_entry) lock_valid() bool {
  if e.load_values() != nil {
    e.Lock()
    return true
  }
  return false
}

// Checks if this value is present
func (e *map_entry) valid() bool {
  return e.values != nil
}

// Will iterate through the linked list, and return a value if the key and hash match.
func (e *map_entry) get(hash uint64, key interface{}) (val interface{}, ok bool) {
  if e.lock_valid() {
    v := e.load_values()
    for v != nil {
      if v.hash == hash && Equal(v.key, key) {
        e.Unlock()
        return v.val, true
      }
      v = v.next
    }
    e.Unlock()
  }
  return nil, false
}

// Appends an item to the linked list
func (e *map_entry) set(hash uint64, key, val interface{}) (add_length bool) {
  e.Lock()
  v := e.load_values()
  if v == nil {
    e.store_values(&map_value{
      hash: hash,
      key: key,
      val: val,
    })
    add_length = true
  } else {
    for {
      if v.hash == hash && Equal(v.key, key) {
        // We are replacing an entry
        v.val = val
        add_length = false
        break
      }
      if v.next == nil {
        v.next = &map_value{
          hash: hash,
          key: key,
          val: val,
        }
        add_length = true
        break
      }
      v = v.next
    }
  }
  e.Unlock()
  return
}

func (e *map_entry) load_values() *map_value {
  return (*map_value) (atomic.LoadPointer(&e.values))
}

func (e *map_entry) store_values(v *map_value) {
  atomic.StorePointer(&e.values, unsafe.Pointer(v))
}
