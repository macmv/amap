# AMap - Atomic Map for Go

Golang has two builtin maps: `sync.Map` and the native `map` type. The native type is fine, but does not
work for multithreading. `sync.Map` works with multithreading, but is very slow (see the benchmarks).
This map aims to fix that: it is a fast, atomic map for go, which doesn't use any mutexes.

### TODO

* [X] More tests. I need to actually make sure this thing works with multiple threads.
* [ ] Removing items. This is a no-brainer. I need a `Del(key interface{})` function.
* [ ] Iterators! I need an `Iter()` function, so that you can range over the map.
* [ ] Set if absent. I already have a skeleton function, I just need to implement it.
* [ ] Switch out the linked list? It might be faster to just shift the other elements in the map data down by one,
instead of storing a linked list. Dereferencing pointers is slow, and I need to do that for every iteration in the list.
* [ ] Smarter grow? It might be faster to always grow any time there is a collision in hashes. This can be problematic
in some situations, but it will be faster to access than a linked list.
* [ ] Faster hash function? Currently, siphash is used for variable length values. This is slow, compared to int hashes,
which use assembly to be very fast.
* [ ] Shrinking? I probably won't add this, but (currently) if the map gets far under it's capacity, it will stay the same size.
The main reason I probably won't add this is what's the usecase? When are you going to want the map to shrink, without
letting it be garbage collected? I just don't think it's worth my time.

### Usage

Example:

```go
package main

import (
  "fmt"

  "gitlab.com/macmv/amap"
)

func main() {
  // Make a typed map (will only accept 'string' and 'int').
  m, err := amap.New("", 0)
  if err != nil { panic(err) }

  // Make an untyped map (can use anything for both key and value).
  u := amap.NewUntyped()

  // Insert items
  m.Set("Hello", 32)
  m.Set("World", 121)

  // err will be non-nil, and the map will be unchanged.
  err = m.Set(5, "Five")

  // Terrible practice, please never do this, but technically possible.
  u.Set(5, "I am another type")
  u.Set(uint32(10), int8(23))

  // Retrieve items.
  fmt.Println(m.Get("Hello"))

  // Check if an item exists.
  val, ok := m.GetOk("World")
  if ok {
    fmt.Println("World:", val)
  }

  // If it doesn't exist, val will always be nil
  val, ok := m.GetOk("String")
  if ok {
    fmt.Println("String:", val)
  } else {
    if val != nil {
      panic("This should never happen!")
    }
  }

  // Sadly, since generics are not a thing yet, you must cast your types:
  num := m.Get("Hello").(int)
  fmt.Println(num > 5)
  // This is why you should not use multiple types in a map.
  // .(int) will panic if the value is not an int.
}
```

By default, `AMap` is typed. The `New()` constructor takes an 'example' key and value. These are passed
to `reflect.TypeOf`, and stored for the duration of the map. If the key cannot be hashed, then `New()` will
return an error. Note that these example values are not added to the map; the new map will be empty.

If you with to use an untyped map, call `NewUntyped()`. This cannot return an error, and it will make `Set()`
slightly faster (by about 20%, see the benchmarks). Using the map in typed form is probably best for development,
unless you actually need to use multiple types.

Setting items is simple: `Set(key, val interface{}) error`. Set will return an error in a couple situations:
If the map is typed, and either the key or value does not match the given type to `New()`, then it
will return an error. If the map is not typed, and the key cannot be hashed, then it will return an error.
Any time `Set()` returns an error, it will not modify the map.

Getting items is a little more annoying. In go, it is useful to know if an item exists in a map. But, it
also looks nice to do this: `m["Hello"] == 5`. For these reasons, there are two getter methods:
`Get(key interface{}) interface{}` and `GetOk(key interface{}) (val interface{}, ok bool)`. `Get()`
is just two lines of code; it simply calls `GetOk()` and returns the value.

`GetOk()` will never check the type of the key. This means it is always the same speed on typed and untyped
maps. `GetOk()` will first try to hash the key. If it can, then it calls `Equal` to check if they are the same.
If they are, it returns the value. See more below for what I mean with `Hash` and `Equal`.

### Key Interface

AMap allows users to use any key type they want. If that key is a number, then it will be hashed with
the builtin hashing algorithm. If the key is a `string` or `[]byte`, then it will be hashed with
[siphash](https://github.com/dchest/siphash). If it is any of those types, then it will also use the builtin `==`
to compare them. Above, when I refer to `Hash` and `Equal`, I am referring to this process of getting
the type of the value, and then doing one of these actions.

If the key is none of those types, then it will be cast to a `Key`:

```go
type Key interface {
  Hash() uint64
  Equal(other interface{}) bool
}
```

The first function, `Hash()`, will be called whenever a `Key` is passed to `Get()` or `Set()`. This
hash function should return a consistent hash of whatever contents the object holds. The function
`amap.HashUint64(v uint64) uint64` is there to help make hashing easier. For example, a simple point
struct might have a hash function like this:

```go
type Point struct {
  x int32
  y int32
}

// The best option. Fast, and you don't
// have issues with x and y swapping.
func (p Point) Hash() uint64 {
  // Place the two int32s next to each other, and hash that.
  return amap.HashUint64(uint64(p.x) << 32 | uint64(p.y))
}

// Larger values, cannot put the x and y
// values next to each other.
type LargePoint struct {
  x int64
  y int64
}

// When x and y are swapped, this returns the same hash.
// This works, but not very well.
func (p LargePoint) Hash() uint64 {
  // x-or the x and y values.
  return uint64(p.x ^ p.y)
}

// Perfectly acceptable. I would use this.
func (p LargePoint) Hash() uint64 {
  // Hash the x value, and then x-or it with the y value.
  return amap.HashUint64(uint64(p.x)) ^ p.y
}

// A more correct, but slower function. I wouldn't
// use this, simply because I feel the second hash
// is unnecessary.
func (p LargePoint) Hash() uint64 {
  // Get the x-or of the two hashes of x and y
  return amap.HashUint64(uint64(p.x)) ^ amap.HashUint64(uint64(p.y))
}
```

All of these functions have something in common: they always return the same thing. If a `Hash()`
function ever returns inconsistent values, then `AMap` will not work at all.

The other function, `Equal()`, is also called during `Get()` and `Set()`. In `Get()`, if the hash
of two keys are the same, then the keys are compared with `Equal()`. `Equal()` expects one thing
to be true: if `a.Equal(b)` then `b.Equal(a)` must be true. The map never verifies this, so you will
get inconsistent results if this is not true.

If the key exists in the map, `Equal()` will always be called at least once whenever you call `Get()`.
In `Set()`, `Equal()` is called if another value has the same hash. So every time you override a value,
`Equal()` will be called. All of this means that `Equal()` is called very often, so it should be fast.

Due to the nature of go's `interface{}`, type casting is slow. Not very slow, but slow enough that
using the `Key` interface will add significant overhead. If you need a very fast map, use `uint64`
as keys, and make the map untyped. I still do not recommend that you make it untyped, as things can
go horribly wrong of you add the wrong type to the map.

### Benchmarks

Run `./bench.sh .` to see results for yourself. This script will host the results on a local server,
which you can access by visiting [http://localhost:8000](http://localhost:8000) on a browser.
The server is hosted on `0.0.0.0`, so if you are running the benchmark on a remote machine, you
can still view it.

On a t3a.medium, I got these results:

```bash
$ ./bench.sh .
goos: linux
goarch: amd64
pkg: gitlab.com/macmv/amap
BenchmarkReadGoMap-2            16399752              72.7 ns/op
BenchmarkReadAMap-2              4475242               283 ns/op
BenchmarkReadSyncMap-2           2029083               603 ns/op
BenchmarkWriteGoMap-2            6135442               201 ns/op
BenchmarkWriteAMap-2             1655192               703 ns/op
BenchmarkWriteAMapUntyped-2      2140076               565 ns/op
BenchmarkWriteSyncMap-2           659419              1896 ns/op
PASS
ok      gitlab.com/macmv/amap   14.489s
Serving web UI on http://0.0.0.0:8000
http://0.0.0.0:8000
```
