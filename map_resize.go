package amap

import (
  "unsafe"
  "sync/atomic"
)

// Doubles the capacity of the map
func (a *AMap) grow() {
  // Lock the map data for writing (only should be used here)
  a.rw.Lock()
  // Replace a.map_data with nil. This 'locks' the map for resizing.
  old_data := (*map_data) (atomic.SwapPointer(&a.map_data, nil))

  // Doubles the size
  keysize := old_data.keysize + 1

  new_data := map_data{
    list: make([]*map_entry, 1 << keysize),
    len: old_data.len,
    keysize: keysize,
  }
  for i := range new_data.list {
    new_data.list[i] = &map_entry{}
  }

  for i := 0; i < 1 << old_data.keysize; i++ {
    old_entry := old_data.list[i]
    if !old_entry.valid() {
      continue
    }
    value := old_entry.load_values()
    for value != nil {
      new_index := new_data.index_of_hash(value.hash)
      new_entry := new_data.list[new_index]
      new_entry.set(value.hash, value.key, value.val)
      value = value.next
    }
  }

  // Replace a.map_data with the new map data.
  atomic.StorePointer(&a.map_data, unsafe.Pointer(&new_data))
  // Make sure to unlock after the pointer is set.
  a.rw.Unlock()
}
