package amap

import (
  "testing"

  "github.com/stretchr/testify/assert"
)

type test_struct struct {}
func (t test_struct) Hash() uint64 { return 5 }
func (t test_struct) Equal(o interface{}) bool { _, ok := o.(test_struct); return ok }

type test_int int32
func (t test_int) Hash() uint64 { return 3 }
func (t test_int) Equal(o interface{}) bool { v, ok := o.(test_int); return ok && t == v }

func TestHash(t *testing.T) {
  v, err := Hash(5)
  assert.Nil(t, err, "Hashing a number should not error")
  // assert.Equal(t, uint64(0x239fee22c504d58a), v, "Hashing a number should return a hash")

  v, err = Hash("Hello")
  assert.Nil(t, err, "Hashing a string should not error")
  // assert.Equal(t, uint64(0x51cf5c3bc87bacc8), v, "Hashing a string should return a hash")

  v, err = Hash([]byte{4, 65, 184, 1})
  assert.Nil(t, err, "Hashing a []byte should not error")
  // assert.Equal(t, uint64(0x9ea3b757c82f623d), v, "Hashing an []byte should return a hash")

  v, err = Hash(test_struct{})
  assert.Nil(t, err, "Hashing a struct with Hash() should not error")
  assert.Equal(t, uint64(5), v, "Hashing a struct with Hash() should return the output of Hash()")

  v, err = Hash(test_int(10))
  assert.Nil(t, err, "Hashing an int with Hash() should not error")
  assert.Equal(t, uint64(3), v, "Hashing an int with Hash() should return the output of Hash(), not the hash of the number")
}

