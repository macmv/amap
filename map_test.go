package amap

import (
  "time"
  "sync"
  //"unsafe"
  "testing"
  //"sync/atomic"

  "github.com/stretchr/testify/assert"
)

// Sanity check for map entry
func TestMapEntry(t *testing.T) {
  // e := &map_entry{}
  // assert.Equal(t, false, e.valid(), "Empty entry should be invalid")
  // assert.Equal(t, true, e.valid(), "Empty entry should be invalid")
}

// This uses set_hashed to verify that collisions are handled properly.
func TestCollisions(t *testing.T) {
  m, _ := New(0, 0)
  m.set_hash(1, 1, 1)
  m.set_hash(1, 2, 2)
  m.set_hash(2, 3, 3)
  assert.Equal(t, `MapData {
  0: {}
  1: {1: 1, -> {2: 2}}
  2: {3: 3}
  3: {}
  4: {}
  5: {}
  6: {}
  7: {}
}`, m.DataString(), "Collisions should cause the hashes to stay ordered")

  v, ok := m.get_hash(1, 1)
  assert.Equal(t, true, ok)
  assert.Equal(t, 1, v)
  v, ok = m.get_hash(1, 2)
  assert.Equal(t, true, ok)
  assert.Equal(t, 2, v)
  v, ok = m.get_hash(2, 3)
  assert.Equal(t, true, ok)
  assert.Equal(t, 3, v)
}

// Simple usecase of the map, on one
// thread. This is a sanity check for
// the linked list and hash functions.
func TestSimpleTyped(t *testing.T) {
  m, err := New("", 0)
  assert.Nil(t, err, "Creating a map should not return an error")

  assert.Equal(t, uint64(0), m.Len(), "Length should be 0 after New()")

  err = m.Set("hello", 5)
  assert.Nil(t, err, "Setting an item should not return an error")
  assert.Equal(t, uint64(1), m.Len(), "Length should be 1 after one set")

  err = m.Set("hello", 3)
  assert.Nil(t, err, "Overriding a value should not return an error")
  assert.Equal(t, uint64(1), m.Len(), "Length should be 1 after an override")

  err = m.Set(5, 5)
  assert.NotNil(t, err, "Setting an item with the wrong type should error")
  assert.Equal(t, uint64(1), m.Len(), "After a failed set, the length should stay the same")

  v, ok := m.GetOk("hello")
  assert.Equal(t, true, ok, "Getting an item should return true")
  assert.Equal(t, 3, v, "Getting an item should be the same as what was entered")

  m.Set("hello", -123)
  v, ok = m.GetOk("hello")
  assert.Equal(t, true, ok, "Getting an item that exists should return true")
  assert.Equal(t, -123, v, "Getting an item should be the same as what was overriden")

  v, ok = m.GetOk(5)
  assert.Equal(t, false, ok, "Getting an item with the wrong type should return false")

  v, ok = m.GetOk("gaming")
  assert.Equal(t, false, ok, "Getting an item with the wrong key should return false")

  //prev, ok := m.Del("gaming")
  //assert.Equal(t, ok, false, "Deleting an item that does not exist should return false")
  //assert.Equal(t, uint64(1), m.Len(), "After a failed Del(), length should be the same")
  //
  //prev, ok = m.Del("hello")
  //assert.Equal(t, ok, true, "Deleting an item that does exist should return true")
  //assert.Equal(t, -123, prev, "Deleting an item should return the correct previous value")
  //assert.Equal(t, uint64(0), m.Len(), "After a working Del(), length should go down by one")
}

// This will test the grow functionality
// of AMap. This is still single threaded.
func TestGrow(t *testing.T) {
  t.Run("DataString", func(t *testing.T) {
    t.Skip("Hashes should change, this is not a good test")
    m, err := New("", 0)
    assert.Nil(t, err, "Creating a map should not return an error")

    assert.Equal(t, uint64(8), m.Cap(), "Cap should be 8 after New()")

    m.Set("lots", 1)
    m.Set("of", 2)
    m.Set("words", 3)
    m.Set("here", 4)

    assert.Equal(t, uint64(8), m.Cap(), "Cap should be 8 after 4 sets")
    assert.Equal(t,
`MapData {
  0: {hash: 15214887783823957552, index: 0, of: 2}
  1: {hash: 15330936931652499417, index: 1, lots: 1}
  2: {}
  3: {}
  4: {hash: 9102075673546566724, index: 4, here: 4}
  5: {}
  6: {}
  7: {hash: 13994050632739850935, index: 7, words: 3}
}`, m.DataString(), "Map data string should be correct")

    m.Set("grow!", 5)

    assert.Equal(t,
`MapData {
  0: {hash: 15214887783823957552, index: 0, of: 2}
  1: {}
  2: {hash: 9314305193193536066, index: 2, grow!: 5}
  3: {}
  4: {hash: 9102075673546566724, index: 4, here: 4}
  5: {}
  6: {}
  7: {hash: 13994050632739850935, index: 7, words: 3}
  8: {}
  9: {hash: 15330936931652499417, index: 9, lots: 1}
  10: {}
  11: {}
  12: {}
  13: {}
  14: {}
  15: {}
}`, m.DataString(), "Map data string should be correct")

    assert.Equal(t, uint64(16), m.Cap(), "Cap should be 16 after 5 sets")

    assert.Equal(t, 1, m.Get("lots"), "Items should be the same after a grow")
    assert.Equal(t, 2, m.Get("of"), "Items should be the same after a grow")
    assert.Equal(t, 3, m.Get("words"), "Items should be the same after a grow")
    assert.Equal(t, 4, m.Get("here"), "Items should be the same after a grow")
    assert.Equal(t, 5, m.Get("grow!"), "Items should be the same after a grow")
  })

  t.Run("Lots", func(t *testing.T) {
    m, _ := New(0, 0)
    const TEST_SIZE = 128
    for j := 0; j < TEST_SIZE; j++ {
      m.Set(j, j)
      // After each addition, we make sure the map is valid
      for k := 0; k < j; k++ {
        val := m.Get(k)
        if k != val {
          t.Log("Map:", m.DataString())
          t.Log("Item is not correct. Expected", k, "got", val)
          t.FailNow()
        }
      }
    }
  })
}

// Tests multithreading
func TestMultithreadAMap(t *testing.T) {
  t.Skip("This doesn't really test anything")
  m, _ := New(0, 0)
  var wg sync.WaitGroup
  wg.Add(2)

  // Write for 5 seconds
  go func() {
    start := time.Now()
    for time.Since(start) < 5 * time.Second {
      m.Set(3, 2)
    }
    wg.Done()
  }()

  // Read for 5 seconds
  go func() {
    start := time.Now()
    for time.Since(start) < 5 * time.Second {
      m.Get(1)
    }
    wg.Done()
  }()

  wg.Wait()
}

// Tests multithreading
func TestMultithreadGoMap(t *testing.T) {
  t.Skip("Skipping, as this test is going to panic (edit the source if you wish to use this test)")

  m := make(map[int]int)
  var wg sync.WaitGroup
  wg.Add(2)

  // Write for 5 seconds
  go func() {
    start := time.Now()
    for time.Since(start) < 5 * time.Second {
      m[3] = 2
    }
    wg.Done()
  }()

  // Read for 5 seconds
  go func() {
    start := time.Now()
    for time.Since(start) < 5 * time.Second {
      _ = m[2]
    }
    wg.Done()
  }()

  wg.Wait()
}

// Tests the shifting part of the Set() function.
func TestLotsAMapSet(t *testing.T) {
  const TEST_SIZE = 2 << 8
  m, _ := New(0, 0)

  for i := 0; i < TEST_SIZE; i++ {
    m.Set(i, i)
  }

  assert.Equal(t, uint64(TEST_SIZE), m.Len(), "Multiple threads should create a map of the correct length")
  for i := 0; i < TEST_SIZE; i++ {
    assert.Equal(t, i, m.Get(i), "Correct value should be written to map")
  }
}

// Multiple threads setting to AMap
func TestMultithreadAMapSet(t *testing.T) {
  const TEST_SIZE = 2 << 8

  m, _ := New(0, 0)
  var wg sync.WaitGroup
  wg.Add(2)

  // Write all even values
  go func() {
    for i := 0; i < TEST_SIZE; i++ {
      if i % 2 == 0 {
        m.Set(i, i)
      }
    }
    wg.Done()
  }()

  // Write all odd values
  go func() {
    for i := 0; i < TEST_SIZE; i++ {
      if i % 2 == 1 {
        m.Set(i, i)
      }
    }
    wg.Done()
  }()

  wg.Wait()

  assert.Equal(t, uint64(TEST_SIZE), m.Len(), "Multiple threads should create a map of the correct length")
  for i := 0; i < TEST_SIZE; i++ {
    assert.Equal(t, i, m.Get(i), "Correct value should be written to map")
  }
}

// Multiple threads deleting from AMap
// WIP: Changed data structure, will fix once Set() is working.
//func TestMultithreadAMapDelete(t *testing.T) {
//  TEST_SIZE := 2 << 12
//
//  m, _ := New(0, 0)
//  for i := 0; i < TEST_SIZE; i++ {
//    m.Set(i, 1)
//  }
//
//  var wg sync.WaitGroup
//  wg.Add(2)
//
//  go func() {
//    // Remove the data, as if we were resizing
//    for i := 0; i < TEST_SIZE; i++ {
//      // Set to nil
//      old := *(*map_data) (atomic.SwapPointer(&m.map_data, nil))
//      // Wait
//      time.Sleep(100 * time.Nanosecond)
//      // Copy the map data
//      new_data := &map_data{
//        list: make([]*map_value, len(old.list)),
//        keysize: old.keysize,
//      }
//      for i, v := range old.list {
//        new_data.list[i] = v
//      }
//      // Put the new data in
//      atomic.StorePointer(&m.map_data, unsafe.Pointer(new_data))
//    }
//    wg.Done()
//  }()
//
//  go func() {
//    // Delete while growing
//    for i := 0; i < TEST_SIZE; i++ {
//      m.Del(i)
//    }
//    wg.Done()
//  }()
//
//  wg.Wait()
//
//  assert.Equal(t, uint64(0), m.Len(), "Multiple threads should create a map of the correct length")
//}
