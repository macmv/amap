package amap

import (
  "errors"
  "unsafe"
  "github.com/dchest/siphash"
)

// echo "amap" | sha1sum
// gives f11d743d23ee0bed94c413bd2f43673e5fed7975
const hash_key_0 = uint64(0xf11d743d23ee0bed)
const hash_key_1 = uint64(0x94c413bd2f43673e)

// Pointer to a uint64. Used in uint64_hash
var key_ptr unsafe.Pointer

func init() {
  num := hash_key_0
  key_ptr = unsafe.Pointer(&num)
}

// Interface for map keys that are not a base type.
type Key interface {
  // Called whenever AMap.Get() is called.
  Equal(other interface{}) bool
  // Calleed whenever AMap.Set() or AMap.Get() is called.
  Hash() uint64
}

// This hashes a lot of types, and it will try two methods of doing so:
// If the type is a Hasher(), it will call Hash(), and return the results.
// Otherwise, it will check if the type is a []byte, string, or number.
// If it is, it will pass that into crc64.Checksum().
func Hash(key interface{}) (uint64, error) {
  // We use siphash for variable length keys,
  // otherwise we use the assembly based hash
  // function. That will try to use a direct
  // hardware hash function, which is very fast.
  switch v := key.(type) {
  case []byte:
    return siphash.Hash(hash_key_0, hash_key_1, v), nil
  case string:
    return siphash.Hash(hash_key_0, hash_key_1, []byte(v)), nil
  case int8:
    return HashUint64(uint64(v)), nil
  case int16:
    return HashUint64(uint64(v)), nil
  case int32:
    return HashUint64(uint64(v)), nil
  case int64:
    return HashUint64(uint64(v)), nil
  case int:
    return HashUint64(uint64(v)), nil
  case uint8:
    return HashUint64(uint64(v)), nil
  case uint16:
    return HashUint64(uint64(v)), nil
  case uint32:
    return HashUint64(uint64(v)), nil
  case uint64:
    return HashUint64(uint64(v)), nil
  case uint:
    return HashUint64(uint64(v)), nil
  case uintptr:
    return HashUint64(uint64(v)), nil
  }
  // Checking this is slow, so we only
  // want to do it if the type is not a builtin.
  h, ok := key.(Key)
  if ok {
    return h.Hash(), nil
  }
  return 0, errors.New("Invalid type!")
}

// Defined in hash.s
func hash_uint64(p unsafe.Pointer, h uint64) uint64

// Can hash a uint64. Indented to be used
// within custom Hash() functions, as this
// is a very fast function.
func HashUint64(v uint64) uint64 {
  return hash_uint64(key_ptr, v)
}

