module gitlab.com/macmv/amap

go 1.15

require (
	github.com/dchest/siphash v1.2.2
	github.com/stretchr/testify v1.7.0
)
