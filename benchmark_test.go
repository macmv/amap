package amap

import (
  "sync"
  "testing"
)

const BENCH_MAP_SIZE = 1024

func BenchmarkReadGoMap(b *testing.B) {
  m := make(map[uint64]uint64)
  var mut sync.Mutex
  for i := uint64(0); i < BENCH_MAP_SIZE; i++ {
    m[i] = i
  }

  b.ResetTimer()

  for i := 0; i < b.N; i++ {
    var wg sync.WaitGroup
    wg.Add(2)
    go func() {
      for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
        mut.Lock()
        if m[j] != j {
          b.Fatal("Got the wrong value from map!")
        }
        mut.Unlock()
      }
      wg.Done()
    }()
    go func() {
      for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
        mut.Lock()
        if m[j] != j {
          b.Fatal("Got the wrong value from map!")
        }
        mut.Unlock()
      }
      wg.Done()
    }()
    wg.Wait()
  }
}

func BenchmarkReadAMap(b *testing.B) {
  m, err := New(uint64(0), uint64(0))
  if err != nil { b.Fatal(err) }
  for i := uint64(0); i < BENCH_MAP_SIZE; i++ {
    m.Set(i, i)
  }

  b.ResetTimer()

  for i := 0; i < b.N; i++ {
    var wg sync.WaitGroup
    wg.Add(2)
    go func() {
      for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
        if m.Get(j) != j {
          b.Fatal("Got the wrong value from map!")
        }
      }
      wg.Done()
    }()
    go func() {
      for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
        if m.Get(j) != j {
          b.Fatal("Got the wrong value from map!")
        }
      }
      wg.Done()
    }()
    wg.Wait()
  }
}

func BenchmarkReadSyncMap(b *testing.B) {
  m := &sync.Map{}
  for i := uint64(0); i < BENCH_MAP_SIZE; i++ {
    m.Store(i, i)
  }

  b.ResetTimer()

  for i := 0; i < b.N; i++ {
    var wg sync.WaitGroup
    wg.Add(2)
    go func() {
      for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
        v, ok := m.Load(j)
        if !ok || v != j {
          b.Fatal("Got the wrong value from map!")
        }
      }
      wg.Done()
    }()
    go func() {
      for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
        v, ok := m.Load(j)
        if !ok || v != j {
          b.Fatal("Got the wrong value from map!")
        }
      }
      wg.Done()
    }()
    wg.Wait()
  }
}

func BenchmarkWriteGoMap(b *testing.B) {
  m := make(map[uint64]uint64)
  num := uint64(0)
  for i := 0; i < b.N; i++ {
    for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
      // Make sure to reassign keys to new values
      m[j] = num
      num++
    }
  }
}

func BenchmarkWriteAMap(b *testing.B) {
  m, err := New(uint64(0), uint64(0))
  if err != nil { b.Fatal(err) }

  num := uint64(0)
  for i := 0; i < b.N; i++ {
    for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
      // Make sure to reassign keys to new values
      m.Set(j, num)
      num++
    }
  }
}

// Untyped is faster, but only for writes.
// There is no type checking on reads,
// so we don't have a benchmark for that.
func BenchmarkWriteAMapUntyped(b *testing.B) {
  m := NewUntyped()

  num := uint64(0)
  for i := 0; i < b.N; i++ {
    for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
      // Make sure to reassign keys to new values
      m.Set(j, num)
      num++
    }
  }
}

func BenchmarkWriteSyncMap(b *testing.B) {
  m := &sync.Map{}

  num := uint64(0)
  for i := 0; i < b.N; i++ {
    for j := uint64(0); j < BENCH_MAP_SIZE; j++ {
      // Make sure to reassign keys to new values
      m.Store(j, num)
      num++
    }
  }
}

func BenchmarkGrowAMap(b *testing.B) {
  for i := 0; i < b.N; i++ {
    b.StopTimer()
    m := NewUntyped()
    // Make a decently large map, so growing will be slow
    for j := 0; j < 1024; j++ {
      m.Set(j, 1)
    }
    b.StartTimer()
    m.grow()
  }
}
