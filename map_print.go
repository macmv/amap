package amap

import (
  "fmt"
  "bytes"
)

// This returns the map data printed in a way
// that helps with debugging. It will show all
// elements of the underlying array, and each
// of those elements will be a linked list.
func (a *AMap) DataString() string {
  return a.load_data().String()
}

// Pretty print. Will print each value on a line.
func (m map_data) String() string {
  var buf bytes.Buffer

  buf.WriteString("MapData {\n")
  for i, val := range m.list {
    buf.WriteString(fmt.Sprintf("  %d: %v\n", i, val.String()))
  }
  buf.WriteString("}")

  return buf.String()
}

// Pretty print. Will print each value in
// a list seperated by ->
func (e *map_entry) String() string {
  if !e.valid() {
    return "{}"
  }

  e.Lock()
  v := e.load_values()
  str := v.String()
  e.Unlock()

  return str
}

func (m *map_value) String() string {
  str := fmt.Sprintf("{%v: %v", m.key, m.val)
  if m.next != nil {
    str += ", -> " + m.next.String()
  }
  str += "}"
  return str
}
