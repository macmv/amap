package amap

import (
  "sync"
  "unsafe"
  "reflect"
  "sync/atomic"
)

// Atomic map. Faster than sync.Map, and can be accessed
// from multiple threads.
type AMap struct {
  // This is a pointer to the map data. This pointer
  // is only changed when the map has to grow.
  // If this ever points to nil, then the map is
  // resizing, and you should try again in a few
  // microseconds.
  map_data unsafe.Pointer

  // This is a lock for growing the map. It should only
  // be locked for writing during grow(). Otherwise, it
  // should be read locked.
  rw *sync.RWMutex

  // These are set on New().
  key_type reflect.Type
  val_type reflect.Type
  // If this is true, then any time Set() or Get()
  // is called, the type of the values passed
  // in will be checked.
  force_type bool
}

// This creates a new AMap.
// This uses the values passed in to set the type.
// This will simply call reflect.TypeOf(v).
// values are not added to the map. If Hash(key)
// returns an error, then this function returns nil, err
func New(key, val interface{}) (*AMap, error) {
  _, err := Hash(key)
  if err != nil { return nil, err }

  a := AMap{}
  a.key_type = reflect.TypeOf(key)
  a.val_type = reflect.TypeOf(val)
  a.force_type = true
  a.rw = &sync.RWMutex{}

  data := map_data{
    // 8 is the default size of a map
    list: make([]*map_entry, 8),
    // 2^3 = 8
    keysize: 3,
  }
  for i := range data.list {
    data.list[i] = &map_entry{}
  }
  // Store the map data
  atomic.StorePointer(&a.map_data, unsafe.Pointer(&data))
  return &a, nil
}

// This creates a new AMap, which does not perform type checks.
// You still must call this instead of &AMap{},
// as the atomic list needs to be initialized.
func NewUntyped() (*AMap) {
  a := AMap{}
  a.force_type = false
  a.rw = &sync.RWMutex{}

  data := map_data{
    // 8 is the default size of a map
    list: make([]*map_entry, 8),
    // 2^3 = 8
    keysize: 3,
  }
  for i := range data.list {
    data.list[i] = &map_entry{}
  }
  // Store the map data
  atomic.StorePointer(&a.map_data, unsafe.Pointer(&data))
  return &a
}

// Atomic load for internal data
func (a *AMap) load_data() *map_data {
  return (*map_data) (atomic.LoadPointer(&a.map_data))
}

// Returns the length of the map.
func (a *AMap) Len() uint64 {
  a.rw.RLock()
  l := a.load_data().Len()
  a.rw.RUnlock()
  return l
}

// Returns the total capacity of the map.
// Will always be a power of 2.
func (a *AMap) Cap() uint64 {
  a.rw.RLock()
  c := a.load_data().Cap()
  a.rw.RUnlock()
  return c
}
