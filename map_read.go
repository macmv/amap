package amap

// This gets a value from the map, but does not show if the value
// existed in the map. Use GetOk if you want to know that.
// This exists because if statements with ok values are ugly,
// and this avoids that.
func (a *AMap) Get(key interface{}) (interface{}) {
  v, _ := a.GetOk(key)
  return v
}

// This retrieves an item from the map. If the key is the wrong type,
// or if the value is not present, it will return nil and false.
// Otherwise, it will return true and a value.
func (a *AMap) GetOk(key interface{}) (interface{}, bool) {
  // Checking type is slower than checking hashes,
  // we never do a type check here.
  hash, err := Hash(key)
  if err != nil { return nil, false }

  return a.get_hash(hash, key)
}

func (a *AMap) get_hash(hash uint64, key interface{}) (interface{}, bool) {
  // Makes sure the map is not resized while reading
  a.rw.RLock()

  data := a.load_data()
  entry := data.list[data.index_of_hash(hash)]
  val, ok := entry.get(hash, key)

  a.rw.RUnlock()

  return val, ok
}
