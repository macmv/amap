package amap

func Equal(a, b interface{}) bool {
  switch v := a.(type) {
  case string:
    return string_equal(v, b)
  case []byte:
    return bytes_equal(v, b)
  case int:
    return int_equal(v, b)
  case int8:
    return int8_equal(v, b)
  case int16:
    return int16_equal(v, b)
  case int32:
    return int32_equal(v, b)
  case int64:
    return int64_equal(v, b)
  case uint:
    return uint_equal(v, b)
  case uint8:
    return uint8_equal(v, b)
  case uint16:
    return uint16_equal(v, b)
  case uint32:
    return uint32_equal(v, b)
  case uint64:
    return uint64_equal(v, b)
  case uintptr:
    return uintptr_equal(v, b)
  }
  k1, ok := a.(Key)
  if !ok { return false }
  k2, ok := b.(Key)
  if !ok { return false }
  return k1.Equal(k2)
}

func string_equal(v string, b interface{}) bool { o, ok := b.(string); return ok && v == o }
// func bytes_equal(v []byte,  b interface{}) bool { o, ok := b.([]byte); return ok && v == o }
func bytes_equal(v []byte,  b interface{}) bool { return false }

func int_equal(v int,         b interface{}) bool { o, ok := b.(int);     return ok && v == o }
func int8_equal(v int8,       b interface{}) bool { o, ok := b.(int8);    return ok && v == o }
func int16_equal(v int16,     b interface{}) bool { o, ok := b.(int16);   return ok && v == o }
func int32_equal(v int32,     b interface{}) bool { o, ok := b.(int32);   return ok && v == o }
func int64_equal(v int64,     b interface{}) bool { o, ok := b.(int64);   return ok && v == o }
func uint_equal(v uint,       b interface{}) bool { o, ok := b.(uint);    return ok && v == o }
func uint8_equal(v uint8,     b interface{}) bool { o, ok := b.(uint8);   return ok && v == o }
func uint16_equal(v uint16,   b interface{}) bool { o, ok := b.(uint16);  return ok && v == o }
func uint32_equal(v uint32,   b interface{}) bool { o, ok := b.(uint32);  return ok && v == o }
func uint64_equal(v uint64,   b interface{}) bool { o, ok := b.(uint64);  return ok && v == o }
func uintptr_equal(v uintptr, b interface{}) bool { o, ok := b.(uintptr); return ok && v == o }
