
mkdir -p pprof

go test . -bench "$@" -cpuprofile pprof/cpu.out && \
  go tool pprof -http 0.0.0.0:8000 amap.test pprof/cpu.out

